
package sequentialsearchlat1;

public class SequentialSearchLat1 {

    public static int sequentialSearch(int[]elements, int target)
    {
        for (int j=0; j<elements.length; j++)
        {
            if (elements[j]==target)
            {
                return j;
            }
        }
        return -1;
    }
    
    public static void main(String[] args) {
        int[] arr1 = {81,3,-20,15,432};
        
        int index = sequentialSearch(arr1,-20);
        System.out.println(index);
        
        index = sequentialSearch(arr1,53);
        System.out.println(index);
    }
    
}
